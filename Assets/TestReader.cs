﻿using UnityEngine;
using System.Collections;
using System;
using System.IO.Ports;


public class TestReader : MonoBehaviour{

	private SerialPort stream;

	public string port;


	void Start () {
		SetupStream();
		StartCoroutine(AsynchronousReadFromMBed((string data) => Debug.Log(data)));
	}

	void SetupStream ()
	{
		stream = new SerialPort(@"\\.\" + this.port, 9600);
		if (stream.IsOpen) {
			stream.Close();
			Debug.Log("Had to close stream...");
		}

		stream.ReadTimeout = 1;
		stream.Open();
		if (stream.IsOpen) {
			Debug.Log("Opened stream!YEAH");
		} 
		else {
			Debug.LogError("Couldn't open stream!!!!");	
		}
	}

	public IEnumerator AsynchronousReadFromMBed(Action<string> callback) {
		DateTime initialTime = DateTime.Now;
		string data = null;
		do {
			data = readDataFromStream();
			if (data != null) {
				callback(data);
				yield return new WaitForSeconds (0.011f);
			} else {
				yield return new WaitForSeconds (0.011f);
			}
		} while (true);
	} 

	string readDataFromStream ()
	{
		string data;
		try {
			data = stream.ReadByte().ToString();
			Debug.Log(data);
		}
		catch (TimeoutException) {
			data = null;
		}
		return data;
	}
}