﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using NSubstitute;
using System;

namespace com.joaopccosta.potentiometer
{
		
	[TestFixture]
	public class PotentiometerModelTest 
	{
		private PotentiometerModelSpy _model;
		private IPotentiometerDataProvider _dataProvider;

		[SetUp]
		public void SetUp () 
		{
			_dataProvider = Substitute.For<IPotentiometerDataProvider> ();
			_model = new PotentiometerModelSpy ();
			_model.Initialise (_dataProvider);
		}

		[Test]
		public void WhenInitialised_DefaultsToOnePotentiometer()
		{
			Assert.That (_model.Values.Count, Is.EqualTo(1));
		}
		
		[Test]
		public void WhenInitialised_AllPotentiometersHaveValueOfZero()
		{
			_model.Initialise (_dataProvider, 10);
			foreach (var potentiometer in _model.Values) 
			{
				Assert.That (potentiometer, Is.EqualTo(0));
			}
		}

		[Test]
		public void WhenDataProviderDispatchesValuesUpdated_ModelUpdatesValuesAndDispatchesEventWithNewValues()
		{
			_model.Initialise (_dataProvider, 10);

			IList<float> updatedValues = new List<float>();

			var invoked = false;
			_model.ValuesUpdated += (values) => 
				{
					invoked = true; 
					updatedValues = values;
				};

			IList<float> fakeValues = new List<float> ();
			fakeValues.Add(10);
			fakeValues.Add(123);
			_dataProvider.ValuesUpdated += Raise.Event<Action<IList<float>>> (fakeValues);

			Assert.That(invoked, Is.True);
			Assert.That(updatedValues[0], Is.EqualTo(fakeValues[0]));
			Assert.That(updatedValues[1], Is.EqualTo(fakeValues[1]));
			Assert.That(updatedValues, Is.EqualTo(_model.Values));
		}
	}

	internal class PotentiometerModelSpy : PotentiometerModel
	{

		public PotentiometerModelSpy () : base ()
		{
		}
		public void UpdateValue(uint potentiometerNumber, uint value)
		{
			base.UpdateValue (potentiometerNumber, value);
		}

		public IList<float> Values
		{
			get
			{
				return base.Values;
			}
		}
	}
}
