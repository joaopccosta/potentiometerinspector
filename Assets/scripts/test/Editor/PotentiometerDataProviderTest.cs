﻿using System;
using NUnit.Framework;
using System.IO.Ports;
using NSubstitute;
using System.Collections.Generic;

namespace com.joaopccosta.potentiometer
{
	[TestFixture]
	public class PotentiometerDataProviderTest
	{


		private PotentiometerDataProviderShunt provider;
		private SerialPort _stream;



		[SetUp]
		public void Setup()
		{
			_stream = Substitute.For<SerialPort>();
			provider = new PotentiometerDataProviderShunt ();
			provider.Stream = _stream;
			//provider.Initialise(_stream);
		}

		[Test]
		[Ignore]
		public void WhenInitialised_OpensStream()
		{
			provider.Setup ("1234");

			// access to COM1 is denied during tests. So I am expecting exception here
			// this is more of an integration test really...
			_stream.Received(1).Open();
		}

		[Test]
		public void WhenCreated_FiresEventWhenReady()
		{
			var ready = false;
			provider.Ready += () => 
			{
				ready = true;
			};
			provider.Setup ("1234");

			Assert.IsTrue (ready);
		}
			
		[Test]
		public void WhenReadDataIsNotNull_DispatchesEventWithNewValues()
		{
			provider.ValuesUpdated += (list) => 
			{
				Assert.That (list, Is.EqualTo (provider.FAKE_STREAM_DATA_VALUES));
			};
			provider.ReadFakeData = true;
			provider.KeepReading(false);
			provider.Setup ("1234");
			provider.StartReading ();
		}
	}

	internal class PotentiometerDataProviderShunt : PotentiometerDataProvider
	{
		public List<uint> FAKE_STREAM_DATA_VALUES = new List<uint> ();

		public bool ReadFakeData = false;

		public PotentiometerDataProviderShunt() : base()
		{
			FAKE_STREAM_DATA_VALUES.Add (10);
			FAKE_STREAM_DATA_VALUES.Add (23);
			FAKE_STREAM_DATA_VALUES.Add (412);
			FAKE_STREAM_DATA_VALUES.Add (0);
		}

		public SerialPort Stream
		{
			set{
				base._stream = value;
			}
		}

		public void KeepReading(bool value)
		{
			base._keepReading = value;
		}

		protected override void CloseStreamIfOpen()
		{
			//Does nothing
		}

		protected override void OpenStream()
		{
			//Does nothing
		}

		protected override string readDataFromStream ()
		{
			if (ReadFakeData) 
			{
				return String.Format("{0};{1};{2};{3};", FAKE_STREAM_DATA_VALUES[0], FAKE_STREAM_DATA_VALUES[1], FAKE_STREAM_DATA_VALUES[2], FAKE_STREAM_DATA_VALUES[3]);
			}

			else
				return base.readDataFromStream ();
		}

		public void InvokeThreadReadingMethod()
		{
			base.ThreadReadingMethod ();
		}


	}
}


