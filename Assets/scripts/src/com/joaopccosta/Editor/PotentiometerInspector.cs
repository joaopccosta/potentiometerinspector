﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using com.joaopccosta.potentiometer;
using System;
using System.Reflection;

/**
 * 
 * VERY EXPERIMENTAL!!!!!
 * Very non production ready code :)
 * This is just a crude experiment to see if this is actually feasible. 
 * 
 * */

[CustomEditor(typeof(PotentiometerDataView))]
public class PotentiometerInspector : Editor {

	[SerializeField]
	public Transform obj;
	[SerializeField]
	public PotentiometerView view;
	[SerializeField]
	public bool active;

	private int selectedOption;
	private int selectedFieldOption;
	private int selectedSubFieldOption;
	private int selectedProvider;
	private string[] providerOptions;

	public override void OnInspectorGUI()
	{		
		view = (PotentiometerView)EditorGUILayout.ObjectField ("PotentiometerView", view, typeof(Component));
		PotentiometerDataView dataView = (PotentiometerDataView)target;

		if (view != null) 
		{
			providerOptions = new string[view.values.Count];
			int i = 0;
			foreach (var value in view.values) 
			{
				providerOptions[i] = String.Format("{0}",i++);
			}

			obj = (Transform)EditorGUILayout.ObjectField ("Transform", obj, typeof(Transform));
			if (obj != null) 
			{
				active = (bool)EditorGUILayout.Toggle ("Active",active);
				if (active) 
				{
					obj.transform.localScale = new Vector3 (view.values [0],view.values [1],view.values [2]);
				}
			}
			this.serializedObject.Update ();

		}
	}
}
