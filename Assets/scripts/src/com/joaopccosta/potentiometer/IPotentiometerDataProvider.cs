using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.IO.Ports;

namespace com.joaopccosta.potentiometer
{
		
	public interface IPotentiometerDataProvider
	{
		
		event Action<IList<float>> ValuesUpdated;
		event Action Ready;

		//void Initialise(SerialPort stream);

		void Setup (string port);
		void StartReading();
	}

}