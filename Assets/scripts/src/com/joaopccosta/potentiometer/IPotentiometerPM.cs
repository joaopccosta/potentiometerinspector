using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using com.joaopccosta.potentiometer;
using System;

public interface IPotentiometerPM
{
	event Action<IList<float>> ValuesUpdated;
}

