﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace com.joaopccosta.potentiometer{

	public class ThreadDestroyer : MonoBehaviour {

		public event Action ApplicationQuit = delegate {};

		void OnApplicationQuit()
		{
			ApplicationQuit ();
		}
	}
}