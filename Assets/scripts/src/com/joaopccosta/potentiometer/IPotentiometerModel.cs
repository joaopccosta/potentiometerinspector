using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace com.joaopccosta.potentiometer
{
		
	public interface IPotentiometerModel
	{
		event Action<IList<float>> ValuesUpdated;

		void Initialise (IPotentiometerDataProvider dataProvider, int numberOfPotentiometers = 1);
	}

}