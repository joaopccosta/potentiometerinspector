﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.joaopccosta.potentiometer;
using Zenject;
using System.IO.Ports;
using System.Threading;
using System;

namespace com.joaopccosta.potentiometer
{
	
[Serializable]
public class PotentiometerView : MonoBehaviour {

	private SerialPort _stream;
	private IPotentiometerPM _pm;

	public string port;

		public List<float> values;

	public event Action ApplicationQuit = delegate {};

	[Inject]
	public void Initialise(IPotentiometerPM pm)
	{
		_pm = pm;
	}
			
	void Start () {
		_pm.ValuesUpdated += OnValuesUpdated;
	}

		void OnValuesUpdated (IList<float> obj)
	{
		var index = 0;
		foreach(var value in obj)
		{
			values [index++] = value;
		}
	}

	private void OnApplicationQuit()
	{
		ApplicationQuit ();
	}
}
}
