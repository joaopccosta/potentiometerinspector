﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using com.joaopccosta.potentiometer;
using System;

namespace com.joaopccosta.potentiometer
{
	public class PotentiometerPM : IPotentiometerPM{

		private IPotentiometerModel _model;
		public event Action<IList<float>> ValuesUpdated = delegate{};

		public event Action ApplicationQuit = delegate{};
		
		[Inject]
		public void Initialise(IPotentiometerModel model)
		{
			_model = model;
			_model.ValuesUpdated += OnValuesUpdated;
		}

		private void OnValuesUpdated (IList<float> obj)
		{
			ValuesUpdated (obj);
		}
	}
}
