﻿using System;
using System.Collections.Generic;
using System.Threading;
using Zenject;
using UnityEngine;

namespace com.joaopccosta.potentiometer
{
	public class FakePotentiometerDataProvider : IPotentiometerDataProvider
	{
		private const char POTENTIOMETER_VALUES_SEPARATOR = ';';

		private int TIME_TO_NEW_DATA_IN_MS = 250;

		public event Action<System.Collections.Generic.IList<float>> ValuesUpdated;

		public event Action Ready;

		protected bool _keepReading = true;
		private Thread worker;

		[Inject]
		public void Initialise(ThreadDestroyer threadDestroyer)
		{
			threadDestroyer.ApplicationQuit += OnApplicationQuit;
			Debug.Log ("Fake Provider set up");
		}

		void OnApplicationQuit ()
		{
			_keepReading = false;
		}

		public void Setup (string port)
		{
			Debug.Log ("Fake Stream set up");
			Ready ();
		}

		public void StartReading ()
		{
			worker = new Thread(ThreadReadingMethod);
			worker.Start ();
		}

		protected virtual void ThreadReadingMethod()
		{
			string data = null;

			List<string> fakedata = new List<string> ();
			fakedata.Add ("0;0;0;");
			fakedata.Add ("10;10;10;");
			fakedata.Add ("30;30;30;");
			fakedata.Add ("20;10;70;");
			fakedata.Add ("10;10;10;");
			fakedata.Add ("23;3;3;");
			fakedata.Add ("10;10;10;");
			fakedata.Add ("12;18;30;");


			int index = 0;
			do
			{
				data = fakedata [index++%fakedata.Count] ;
				if (data != null) 
				{
					Debug.Log(data);
					var list = new List<float> ();
					foreach (var value in data.Split(POTENTIOMETER_VALUES_SEPARATOR)) 
					{
						if (!value.Equals ("")) 
						{
							var i = Convert.ToUInt32 (value.ToString (), 10);
							list.Add (i);
						}
					}

					ValuesUpdated (list);
				}
				Thread.Sleep (TIME_TO_NEW_DATA_IN_MS);
			} 
			while(_keepReading);
		}
	}
}

