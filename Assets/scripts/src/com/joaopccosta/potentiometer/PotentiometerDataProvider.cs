﻿using System;
using System.IO.Ports;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Threading;
using Zenject;

namespace com.joaopccosta.potentiometer
{
	
	public class PotentiometerDataProvider : IPotentiometerDataProvider
	{

		private const char POTENTIOMETER_VALUES_SEPARATOR = ';';

		public event Action<IList<float>> ValuesUpdated = delegate{};
		public event Action Ready = delegate{};

		protected bool _keepReading = true;
		private Thread worker;
		protected SerialPort _stream;

		[Inject]
		public void Initialise(ThreadDestroyer threadDestroyer)
		{
			threadDestroyer.ApplicationQuit += OnApplicationQuit;
		}

		void OnApplicationQuit ()
		{
			_keepReading = false;
			CloseStreamIfOpen ();
		}

		public void Setup (string port)
		{
			SetupStream (port);
			Ready ();
		}

		private void SetupStream (string port)
		{
			if (_stream == null) 
			{
				_stream = new SerialPort (port, 9600);
			}
			_stream.ReadTimeout = 1;

			CloseStreamIfOpen ();
			OpenStream ();

		}

		protected virtual void CloseStreamIfOpen ()
		{
			if (_stream.IsOpen) {
				_stream.Close ();
				Debug.Log("Had to close stream...");
			}
		}

		protected virtual void OpenStream ()
		{
			_stream.Open ();
			VerifyStream ();
		}

		private void VerifyStream ()
		{
			if (_stream.IsOpen) {
				Debug.Log("Opened stream - "+_stream.PortName);
			}
			else {
				Debug.LogError("Couldn't open stream!!!!");	
			}
		}

		public void StartReading ()
		{
			worker = new Thread(ThreadReadingMethod);
			worker.Start ();
		}

		protected virtual void ThreadReadingMethod()
		{
			string data = null;
			bool fake = true;

			int index = 0;
			do
			{
				data = readDataFromStream ();
				if (data != null) 
				{
					Debug.Log(String.Format("DATA {0}", data));
					var list = new List<float> ();
					foreach (string value in data.Split(POTENTIOMETER_VALUES_SEPARATOR)) 
					{
						if (!value.Equals ("")) 
						{
							list.Add (float.Parse(value, System.Globalization.NumberStyles.Float));
						}
					}

					ValuesUpdated (list);
				}
			} 
			while(_keepReading);
		}

		protected virtual string readDataFromStream ()
		{
			string data = "";
			try {
				data = _stream.ReadTo("\n");		

			}
			catch (TimeoutException) {
				data = null;
			}
			return data;
		}
	}
}

