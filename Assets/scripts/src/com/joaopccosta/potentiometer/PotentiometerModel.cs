﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Zenject;

namespace com.joaopccosta.potentiometer
{
		
	public class PotentiometerModel : IPotentiometerModel
	{
		private IDictionary<uint,float> _potentiometers;
		private const float MAX_VALUE = 255.0f;
		private readonly string DEFAULT_SERIAL_PORT = "COM3";

		public event Action<IList<float>> ValuesUpdated = delegate{};

		private IPotentiometerDataProvider _dataProvider;

		[Inject]
		public void Initialise(IPotentiometerDataProvider dataProvider, int numberOfPotentiometers = 1)
		{
			_potentiometers = new Dictionary<uint,float> (numberOfPotentiometers);

			for (uint i = 0; i < numberOfPotentiometers; ++i) 
			{
				_potentiometers [i] = 0;
			}
			_dataProvider = dataProvider;
			_dataProvider.Ready += OnReady;
			_dataProvider.ValuesUpdated += OnValuesUpdated;

			_dataProvider.Setup (DEFAULT_SERIAL_PORT);

		}

		private void OnReady ()
		{
			_dataProvider.StartReading ();
		}
			
		protected IList<float> Values 
		{
			get 
			{
				return _potentiometers.Values.ToList();
			}
		}

		private void OnValuesUpdated (IList<float> obj)
		{
			uint i = 0;
			foreach (var value in obj) 
			{
				UpdateValue (i++, value);
			}				
			ValuesUpdated (Values);
		}

		protected void UpdateValue(uint potentiometerNumber, float value)
		{


			_potentiometers [potentiometerNumber] = 10.0f * value +1.0f;

		}

	}
}