using UnityEngine;
using Zenject;
using com.joaopccosta.potentiometer;
using System.IO.Ports;


public class ContextInstaller : MonoInstaller<ContextInstaller>
{

	public PotentiometerView view;
	public ThreadDestroyer threadDestroyer;

    public override void InstallBindings()
    {
		ThreadDestroyer threadDestroyerInstance = Container.InstantiatePrefab(threadDestroyer).GetComponent<ThreadDestroyer>();

		Container.Bind<ThreadDestroyer> ().FromInstance(threadDestroyerInstance).AsSingle();
		Container.Bind<SerialPort> ().ToSelf().AsSingle();
		Container.Bind<IPotentiometerDataProvider>().To<PotentiometerDataProvider> ().AsSingle ().NonLazy();
		Container.Bind<IPotentiometerModel>().To<PotentiometerModel> ().AsSingle ().NonLazy();
		Container.Bind<IPotentiometerPM>().To<PotentiometerPM> ().AsSingle ().NonLazy();

		Container.InstantiatePrefab (view);
    }
}