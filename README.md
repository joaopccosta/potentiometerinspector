# README #

Project using an RPi3 to change Unity component fields on the fly via potentiometers.

If you look at the commits, the idea for this project started in 2016. It started off as being a project to use an mbed LPC1768 to read potentiometer values and feed those to unity.
Only very recently I had the chance to pick up this project that was left dormant for practically a year.
The mbed LCP1768 code is in a separate folder in this project.

I used Zenject for dependency injection. If you are thinking of using this elsewhere, if you are not using this IoC framework, then you will have to adapt the Context class which is included in this project to your own.

## How to run ##

1. Open the only scene in the project.
2. Create a new inspector tab.
3. Run.
4. Open the PotentiometerView object from the hierarchy.
  a. Drag the PotentiometerView component into the PotentiometerInspector Component field. (I know I need to fix this :) )
5. Drag the Cube object from the hierarchy on to the PotentiometerInspector Transform field.
6. Activate the transform.
7. Use the potentiometers and see the scale of the cube change.

## What is done ##
* DataProvider reads data from a given Serial port, in an expected format. It feeds the data to a model (via events), which bubble up all the way to the component View.
* PotentiometerView displays the read values.
* All classes tested with unit tests.
* A Very experimental custom inspector for unity is provided to try to associate a potentiometer value with a field of a component.
* This custom inspector will change the localscale property of a passed transform component, with the three first values of the provided data.

## What needs to be done ##
* Optimise reflection used in custom inspector script.
* Perhaps give the choice of which components do we want to change instead of limiting the choice to the transform.scale.